import pandas as pd
import os
import psycopg2
from dotenv import load_dotenv

# Chargement des variable d'environnement
load_dotenv()
db_driver = os.getenv("DB_DRIVER")
db_host = os.getenv("DB_HOST")
db_port = os.getenv("DB_PORT")
db_user = os.getenv("DB_USER")
db_password = os.getenv("DB_PASSWORD")
db_name = os.getenv("DB_NAME")

# Connexion à la base de données
connection_string = f"{db_driver}://{db_user}:{db_password}@{db_host}:{db_port}/{db_name}"

try:
    connection = psycopg2.connect(connection_string)
    cursor = connection.cursor()
    cursor.execute("SELECT version();")
    db_version = cursor.fetchone()
    print("Connected to:", db_version)
    cursor.close()
    connection.close()
except (Exception, psycopg2.Error) as error:
    print("Error while connecting to PostgreSQL:", error)

# # Chargement des données d'un fichier de données dans la base de données
# try:
#     connection = psycopg2.connect(connection_string)
#     cursor = connection.cursor()

#     csv_file_path = 'data/your_csv_file.csv'
#     df = pd.read_csv(csv_file_path)

#     # Information à propos de la structure de données de mon ficher csv
#     columns_info = [(column, df[column].dtype) for column in df.columns]

#     # Génération de la table en fonction de la structure du ficheir csv
#     table_name = 'your_table_name'
#     create_table_query = f"CREATE TABLE IF NOT EXISTS {table_name} ("
#     for column_name, column_type in columns_info:
#         create_table_query += f"{column_name} {column_type}, "
#     create_table_query = create_table_query.rstrip(', ') + ");"

#     cursor.execute(create_table_query)
#     connection.commit()

#     # Insértion des données dans la base de données
#     for index, row in df.iterrows():
#         insert_query = f"INSERT INTO {table_name} VALUES ({', '.join(['%s']*len(row))})"
#         cursor.execute(insert_query, row)

#     connection.commit()
#     print("Data inserted successfully!")

#     cursor.close()
#     connection.close()
# except (Exception, psycopg2.Error) as error:
#     print("Error:", error)


# Étape 1 : Sélectionner un fichier de données sur kaggle et réfléchir à un project de création de valuer sur ces données
# Étape 2 : Intégration des donnés, il faut soit split un fichier de données en plusieurs formats et les intégrer dans un datawarehouse 
# soit intégrer des fichiers sous plusieurs formats dans une base de données (minimum 3 types de formats)
# Étape 3 : Effectuer des transformations sur les données en respectant des règles métier que vous allez définir pour votre projet
# Étape 4 : Effectuer des requêtes sql pour afficher des informations (création de valeur) dans divers graphes sur grafana (dataviz)

